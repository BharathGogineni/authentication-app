import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  RefreshControlBase,
} from 'react-native';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationEvents} from 'react-navigation';
import login from './login.js';
class signUp extends Component {
  state = {
    username: '',
    password: '',
    confirmpassword: '',
  };

  handleUserName = (text) => {
    this.setState({username: text});
  };
  handlePassword = (text) => {
    this.setState({password: text});
  };
  handleConfirmPassword = (text) => {
    this.setState({confirmpassword: text});
  };

  displaywebpage = () => {
    this.props.navigation.navigate('page');
  };
  check = () => {
    if (this.state.password == this.state.confirmpassword) {
      this.register();
    } else {
      alert('passwords mismatch');
    }
  };
  async register() {
    const object1 = {
      username: this.state.username,
      password: this.state.password,
    };
    const object2 = JSON.stringify(object1);
    const requestOptions = {
      method: 'POST',
      headers: {'content-type': 'application/json'},
      body: object2,
    };

    try {
      let response = await fetch(
        'http://192.168.0.105:3000/register',
        requestOptions,
      );

      if (response.status == 200) {
        alert('User successfully signedin please login');
        this.props.navigation.navigate('Login');
      } else {
        var obj = await response.json();
        var obj1 = JSON.parse(obj);
        alert(obj1.message);
      }
      // var result = await response.json();
      // console.log(result);
      // if (result.reply == "ok") {
      //     this.props.navigation.navigate('Login');

      // }
      // else {
      //     alert('register with different username');
      // }

      //let json = await response;
      //console.log(json.body);
      //return json.movies;
    } catch (error) {
      console.error(error);
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="User Name"
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
          onChangeText={this.handleUserName}
        />

        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Password"
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
          secureTextEntry={true}
          onChangeText={this.handlePassword}
        />

        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Confirm Password"
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
          secureTextEntry={true}
          onChangeText={this.handleConfirmPassword}
        />

        <TouchableOpacity
          style={styles.submitButton}
          // onPress={() => this.props.navigation.navigate(login)}>
          onPress={() => {
            this.check();
          }}>
          <Text style={styles.submitButtonText}> Submit </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default signUp;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1,
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    marginRight: 190,
    marginLeft: 140,
    marginTop: 10,
    paddingTop: 10,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },
});
