import React, {Component} from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {View, Text, StyleSheet, Button, TextInput, Linking} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from './login.js';
import signUp from './signup.js';
import page from './page.js';

const Stack = createStackNavigator();

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSignedIn: false,
      isloading: false,
    };
  }
  checkPrint = () => {
    console.log('I am able to call');
  };
  componentDidMount() {
    const requestOptions = {
      method: 'POST',
      headers: {'content-type': 'application/json'},
    };

    fetch('http://192.168.0.105:3000/home', requestOptions)
      .then((response) => {
        if (response.status === 302) {
          //no session
          this.setState({isloading: false, isSignedIn: false});
        } else {
          this.setState({isloading: false, isSignedIn: true});
        }
      })
      .catch((error) => {
        alert(error);
      });
    this.setState({isloading: true});
  }
  render() {
    if (this.state.isloading) {
      return (
        <View style={styles.loadingtext}>
          <Text> isloading </Text>
        </View>
      );
    } else {
      return (
        <NavigationContainer>
          <Stack.Navigator>
            {this.state.isSignedIn ? (
              <Stack.Screen
                name="page"
                component={page}
                options={{headerLeft: null}}
              />
            ) : (
              <>
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="signUp" component={signUp} />
                <Stack.Screen
                  name="page"
                  component={page}
                  options={{headerLeft: null}}
                />
              </>
            )}
          </Stack.Navigator>
        </NavigationContainer>
      );
    }
  }
}

const styles = StyleSheet.create({
  loadingtext: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
});
