import React, {Component} from 'react';
import {View} from 'react-native';
import 'react-native-gesture-handler';
import {WebView} from 'react-native-webview';

export default class page extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <WebView
          //source={{ uri: this.props.navigation.state.params.url }}
          source={{uri: 'https://www.gamezy.com/game/#/'}}
          style={{flex: 1}}
        />
      </View>
    );
  }
}
