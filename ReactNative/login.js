import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';
import 'react-native-gesture-handler';

import signUp from './signup.js';
import page from './page.js';
import app from './App.js';

class Login extends Component {
  state = {
    username: '',
    password: '',
  };
  handleUserName = (text) => {
    this.setState({username: text});
  };
  handlePassword = (text) => {
    this.setState({password: text});
  };

  displaywebpage = () => {
    this.props.navigation.navigate('page');
  };

  async login() {
    const object1 = {
      username: this.state.username,
      password: this.state.password,
    };
    const object2 = JSON.stringify(object1);
    console.log(object2);

    const requestOptions = {
      method: 'POST',
      headers: {'content-type': 'application/json'},
      body: object2,
    };

    try {
      let response = await fetch(
        'http://192.168.0.105:3000/login',
        requestOptions,
      );

      if (response.status == 200) {
        this.displaywebpage();
      } else {
        var obj = await response.json();
        var obj1 = JSON.parse(obj);
        alert(obj1.message);
      }
    } catch (error) {
      console.error(error);
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input1}
          underlineColorAndroid="transparent"
          placeholder="User-Name"
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
          onChangeText={this.handleUserName}
        />

        <TextInput
          style={styles.input}
          underlineColorAndroid="transparent"
          placeholder="Password"
          placeholderTextColor="#9a73ef"
          autoCapitalize="none"
          secureTextEntry={true}
          onChangeText={this.handlePassword}
        />

        <TouchableOpacity
          style={styles.submitButton}
          //onPress={() => this.displaywebpage()}>
          onPress={() => this.login()}>
          <Text style={styles.submitButtonText}> Login </Text>
        </TouchableOpacity>
        <Text style={styles.newuser}>newusers?</Text>
        <TouchableOpacity
          style={styles.submitButton}
          onPress={() => this.props.navigation.navigate(signUp)}>
          <Text style={styles.submitButtonText}> Signup </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 2,
    backgroundColor: '#ecf0f1',
    justifyContent: 'center',
  },
  newuser: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 150,
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1,
  },
  input1: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1,
  },
  passwordtext: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1,
  },
  submitButton: {
    backgroundColor: '#7a42f4',
    padding: 10,
    marginRight: 190,
    marginLeft: 140,
    marginTop: 10,
    paddingTop: 10,
    height: 40,
  },
  submitButtonText: {
    color: 'white',
  },
});
