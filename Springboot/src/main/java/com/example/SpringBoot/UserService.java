package com.example.SpringBoot
;
import java.util.List;

import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
@Service
@Transactional
public class UserService {
	
	@Autowired
	private UserRepository repo;
	

   public void save(User user) {
        repo.save(user);
    }
     
    public User get(String username) {
    	 java.util.Optional<User> var1 = repo.findById(username);
    	if(var1.isPresent())
        return repo.findById(username).get();
    	else 
    	return null;
    }
     

}
