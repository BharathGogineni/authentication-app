package com.example.SpringBoot;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.util.*;
import java.lang.String;
import org.springframework.beans.factory.annotation.*;
import org.springframework.http.*;
 
import org.springframework.web.bind.annotation.*;

import com.example.SpringBoot.User;

@RestController	
public class UserController {
	
	@Autowired
	private UserService userservice;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	
	//register
	
	@RequestMapping(method = RequestMethod.POST, value = "/register")
	public ResponseEntity<String> register(@RequestBody User newuser)
	{
		//when username is null
		if(newuser.getUsername().equals(""))
			return new ResponseEntity<>("Enter Username",HttpStatus.UNAUTHORIZED);
		
		//when password is null
		if(newuser.getPassword().equals(""))
			return new ResponseEntity<>("Enter Password",HttpStatus.UNAUTHORIZED);
		
		
		User user = userservice.get(newuser.getUsername());
		if(user==null) {
			String dummy = (bCryptPasswordEncoder.encode(newuser.getPassword()));
			System.out.println("the encrypted password for "+newuser.getPassword()+" is "+ dummy);
			newuser.setPassword(dummy);
			userservice.save(newuser);
			return new ResponseEntity<>("SignUp successful. Please Login",HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<>("Username already exists in the database",HttpStatus.UNAUTHORIZED);
		}
		
	}
	
//	$2a$10$h5hhFJxJBX8OU2aEHalSou6rh2krvuCN.p6AlLnC6P3CwULft328q
	
	
	//login
	
	@RequestMapping(method = RequestMethod.POST, value="/login")
	public ResponseEntity<String> login(@RequestBody User newuser){
		
		//when username is null
		if(newuser.getUsername().equals(""))
			return new ResponseEntity<>("Enter Username",HttpStatus.UNAUTHORIZED);
		
		//when password is null
		if(newuser.getPassword().equals(""))
			return new ResponseEntity<>("Enter Password",HttpStatus.UNAUTHORIZED);
		
		
		User user = userservice.get(newuser.getUsername());

		
		if(user== null) {
			return new ResponseEntity<>("Please signup first to login.",HttpStatus.UNAUTHORIZED);
		}
		else {
			String hashedPassword = (bCryptPasswordEncoder.encode(newuser.getPassword()));
			
			if(!bCryptPasswordEncoder.matches(newuser.getPassword(), user.getPassword()))
			{
				return new ResponseEntity<>("Enter valid Username and Password.",HttpStatus.UNAUTHORIZED);
			}
			else
			{
				return new ResponseEntity<>("Logged in successfully",HttpStatus.OK);
			}
		}
	}


	



	
	

}
