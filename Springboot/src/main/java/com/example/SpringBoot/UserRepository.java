package com.example.SpringBoot;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.example.SpringBoot.User;

public interface UserRepository  extends JpaRepository<User, String>{

}
