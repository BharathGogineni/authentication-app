let express = require("express");
const router = express.Router();
router.post("/", function (req, res) {
  if (!req.session.key) {
    res.status(302).json("login");
  } else {
    res.status(200).json("page");
  }
});

module.exports = router;
