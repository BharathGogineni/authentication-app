const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const session = require("express-session");
const redis = require("redis");
const redisClient = redis.createClient();
const redisStore = require("connect-redis")(session);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

redisClient.on("error", (err) => {
  console.log("Redis error: ", err);
});

app.use(
  session({
    secret: "Thisisappisusedforlogginginandsigningin",
    name: "_redisPractice",
    resave: false,
    saveUninitialized: false,
    cookie: { secure: false, sameSite: true },
    store: new redisStore({
      host: "localhost",
      port: 6379,
      client: redisClient,
      ttl: 120,
    }),
  })
);

app.use("/login", require("./login.js"));
app.use("/register", require("./register.js"));
app.use("/home", require("./home.js"));
app.listen(3000);
