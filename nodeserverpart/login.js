let express = require("express");
let router = express.Router();
var request = require("request");

router.post("/", function (req, res) {
  var body = req.body;

  request.post(
    {
      headers: { Accept: "application/json" },
      url: "http://localhost:8080/login",
      body: body,
      json: true,
    },
    function (error, response, body) {
      var modifiedbody = JSON.stringify({ message: body });
      if (response.statusCode == 200) req.session.key = req.body.username;
      res.status(response.statusCode).json(modifiedbody);
    }
  );
});

module.exports = router;
